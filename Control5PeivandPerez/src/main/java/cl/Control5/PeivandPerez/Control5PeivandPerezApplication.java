package cl.Control5.PeivandPerez;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Control5PeivandPerezApplication {

	public static void main(String[] args) {
		SpringApplication.run(Control5PeivandPerezApplication.class, args);
	}

}
