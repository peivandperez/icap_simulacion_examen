package cl.Control5.PeivandPerez.Controller;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import cl.Control5.PeivandPerez.Model.DTO.NotasDto;
import cl.Control5.PeivandPerez.Service.NotasService;


@RestController
@RequestMapping(value="/notas")
public class NotasController {

	@Autowired
	NotasService notasService;
	
	
	
	@RequestMapping(value="/list")
	public List<NotasDto> ajaxList(HttpServletRequest req, HttpServletResponse res) {
		List<NotasDto> list = notasService.list();
		return list;
	}
	
	@RequestMapping(value="/get")
	public NotasDto ajaxGet(HttpServletRequest req, HttpServletResponse res) {
		return notasService.get(req.getParameter("rut"));	
	}
	
	@RequestMapping(value="/getPromedioAlumno")
	public NotasDto ajaxGetPromedioAlumno(HttpServletRequest req, HttpServletResponse res) {
		NotasDto notasDto = new NotasDto();
		notasDto.setRut(req.getParameter("rut"));
		return notasService.getPromedioAlumno(notasDto);
		
	}
	
	
	
	@RequestMapping(value="/delete")
	public int ajaxDelete(HttpServletRequest req,HttpServletResponse res){
		int rows=0;
		try {
			rows= notasService.delete(Integer.parseInt(req.getParameter("idNota")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rows;	
		}
	
	@RequestMapping(value="/insert")
	public @ResponseBody int ajaxInsert(HttpServletRequest req, HttpServletResponse res) {
		int rows=0;
		try {
			String requestData = req.getReader().lines().collect(Collectors.joining());
			NotasDto notas = new Gson().fromJson(requestData, NotasDto.class);
			rows = notasService.insert(notas);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	
		return rows;
	}
	
	@RequestMapping(value="/update")
	public @ResponseBody int ajaxUpdate(HttpServletRequest req, HttpServletResponse res) {
		int rows=0;
		try {
			String requestData = req.getReader().lines().collect(Collectors.joining());
			NotasDto notas = new Gson().fromJson(requestData, NotasDto.class);
			rows = notasService.update(notas);
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		return rows;
	}
	
	
}
