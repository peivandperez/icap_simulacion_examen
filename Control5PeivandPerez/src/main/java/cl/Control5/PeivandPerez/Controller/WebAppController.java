package cl.Control5.PeivandPerez.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("/WebAppController")
public class WebAppController {

	@RequestMapping("/")
	public String getIndex(){
		return "index";
	}
}
