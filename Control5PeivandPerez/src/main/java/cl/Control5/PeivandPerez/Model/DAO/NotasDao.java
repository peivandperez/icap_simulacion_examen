package cl.Control5.PeivandPerez.Model.DAO;

import java.util.List;

import cl.Control5.PeivandPerez.Model.DTO.NotasDto;

public interface NotasDao {
	public int insert(NotasDto notasDto);
	public NotasDto get(String rut); 
	public int update(NotasDto notasDto); 
	public int delete(int idNota);

	List<NotasDto> listCurso();
	public NotasDto getPromedioAlumno(NotasDto notasDto);
}
