package cl.Control5.PeivandPerez.Model.DAO;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cl.Control5.PeivandPerez.Model.DTO.NotasDto;




@Repository
@Transactional

public class NotasDaoImpl implements NotasDao {
	private String insertNotas="INSERT INTO notas (id_nota,codigo_curso,rut,numero_modulo,numero_evaluacion,nota) "
								+"VALUES (:idNota, :codigoCurso, :rut, :numeroModulo, :numeroEvaluacion, :nota)";
	
	private String updateNotas="UPDATE notas SET numero_modulo= :numeroModulo, numero_evaluacion= :numeroEvaluacion, nota= :nota"
								+" WHERE id_nota= :idNota";
	
	private String deleteNotas="DELETE FROM notas WHERE id_nota= :idNota";
	
	private String selectOneNotasAlumno="SELECT a.rut, a.nombre, c.nombre, n.numero_modulo, n.numero_evaluacion, n.nota, n.id_nota"
										+" FROM alumno a, curso c, notas n"
										+" WHERE a.rut = :rut AND a.rut= n.rut "
										+" AND a.codigo_curso= n.codigo_curso and c.codigo_curso=n.codigo_curso";
	
	private String listSqlCurso="SELECT c.nombre, a.rut, a.nombre, n.numero_modulo, n.numero_evaluacion, n.nota, n.id_nota"
								+"FROM alumno a, notas n "
								+"WHERE a.rut =n.rut AND a.codigo_curso=? "
								+ "AND a.codigo_curso = n.codigo_curso AND c.codigo_curso = n.codigo_curso";
	
	private String listAll= "SELECT c.codigo_curso, c.nombre, a.rut, a.nombre, n.numero_modulo, n.numero_evaluacion, n.nota, n.id_nota"
							+" FROM alumno a, notas n, curso c"
							+" WHERE a.rut =n.rut AND a.codigo_curso=n.codigo_curso AND n.codigo_curso=c.codigo_curso";
	
	private String notaPromedioAlumno="select avg(nota)from notas where rut= :rut";
	
	

			
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Override
	public int insert(NotasDto notasDto) {
		int rows =0;
		
		 
		try {
	
			MapSqlParameterSource params =new MapSqlParameterSource();
	
			params.addValue("idNota", notasDto.getIdNota(), Types.INTEGER);
			params.addValue("codigoCurso", notasDto.getCodigoCurso(),Types.INTEGER);
			params.addValue("rut", notasDto.getRut(),Types.VARCHAR);
			params.addValue("numeroModulo", notasDto.getNumeroModulo(),Types.INTEGER);
			params.addValue("numeroEvaluacion", notasDto.getNumeroEvaluacion(),Types.INTEGER);
			params.addValue("nota", notasDto.getNota(),Types.DOUBLE);
			
	
			rows = namedParameterJdbcTemplate.update(insertNotas, params);
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return rows;
	}

	@Override
	public NotasDto get(String rut) {
		Object args[]= {rut};
		NotasDto notasDto;
		try {
			notasDto= jdbcTemplate.queryForObject(selectOneNotasAlumno, args, BeanPropertyRowMapper.newInstance(NotasDto.class));
			
		} catch (Exception e) {
			notasDto=null;
			e.printStackTrace();
		}
		return notasDto; 
	}
	

	@Override
	public int update(NotasDto notasDto) {
		int rows =0;
		
		 
		try {
	
			MapSqlParameterSource params =new MapSqlParameterSource();
	
			params.addValue("idNota", notasDto.getIdNota(), Types.INTEGER);
			params.addValue("numeroModulo", notasDto.getNumeroModulo(),Types.INTEGER);
			params.addValue("numeroEvaluacion", notasDto.getNumeroEvaluacion(),Types.INTEGER);
			params.addValue("nota", notasDto.getNota(),Types.DOUBLE);
			
	
			rows = namedParameterJdbcTemplate.update(updateNotas, params);
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return rows;
	}

	@Override
	public int delete(int idNota) {
	int rows=0;
		
		Object args[]= {idNota};
	try {
			rows= jdbcTemplate.update(deleteNotas,args);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rows; 
	}

	/*
	@Override
	public List<NotasDto> listCurso(int codigoCurso) {
		
	
		List<Map<String, Object>> listCurso= jdbcTemplate.queryForList(listSqlCurso, codigoCurso);
				//jdbcTemplate.queryForList(listSqlCurso,BeanPropertyRowMapper.newInstance(NotasDto.class));
		
	
		return listCurso;	
	}
	*/
	
	@Override
	public List<NotasDto> listCurso() {
		
		List<NotasDto> listCurso= jdbcTemplate.query(listAll, BeanPropertyRowMapper.newInstance(NotasDto.class));
		return listCurso;
	}
	
	@Override
	public NotasDto getPromedioAlumno(NotasDto notasDto) {
	
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(notasDto);
				
		return namedParameterJdbcTemplate.queryForObject(notaPromedioAlumno, params, NotasDto.class);
	
	}
		/*
		 Object args[]= {notasDto.getRut()};
		 
	
		try {
			notasDto= jdbcTemplate.queryForObject(selectOneNotasAlumno, args, BeanPropertyRowMapper.newInstance(NotasDto.class));
			
		} catch (Exception e) {
			notasDto=null;
			e.printStackTrace();
		}
		return notasDto; 
	}
	*/
}
