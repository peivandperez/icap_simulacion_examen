package cl.Control5.PeivandPerez.Model.DTO;

public class NotasDto {

	private int codigoCurso;
	private String nombreCurso;
	private String rut;
	private String nombreAlumno;
	private int numeroModulo;
	private int numeroEvaluacion;
	private double nota;
	private int idNota;
	private double promedioNota;
	private double promedioCurso;
		
		public int getIdNota() {
			return idNota;
		}
		public void setIdNota(int idNota) {
			this.idNota = idNota;
		}
		public int getCodigoCurso() {
			return codigoCurso;
		}
		public void setCodigoCurso(int codigoCurso) {
			this.codigoCurso = codigoCurso;
		}
		public String getNombreCurso() {
			return nombreCurso;
		}
		public void setNombreCurso(String nombreCurso) {
			this.nombreCurso = nombreCurso;
		}
		public String getRut() {
			return rut;
		}
		public void setRut(String rut) {
			this.rut = rut;
		}
		public String getNombreAlumno() {
			return nombreAlumno;
		}
		public void setNombreAlumno(String nombreAlumno) {
			this.nombreAlumno = nombreAlumno;
		}
		public int getNumeroModulo() {
			return numeroModulo;
		}
		public void setNumeroModulo(int numeroModulo) {
			this.numeroModulo = numeroModulo;
		}
		public int getNumeroEvaluacion() {
			return numeroEvaluacion;
		}
		public void setNumeroEvaluacion(int numeroEvaluacion) {
			this.numeroEvaluacion = numeroEvaluacion;
		}
		public double getNota() {
			return nota;
		}
		public void setNota(double nota) {
			this.nota = nota;
		}
		
		
		public double getPromedioNota() {
			return promedioNota;
		}
		public void setPromedioNota(double promedioNota) {
			this.promedioNota = promedioNota;
		}
		public double getPromedioCurso() {
			return promedioCurso;
		}
		public void setPromedioCurso(double promedioCurso) {
			this.promedioCurso = promedioCurso;
		}
		
		
		
		public NotasDto() {
		}
		
		
		
		@Override
		public String toString() {
			return "NotasDto [codigoCurso=" + codigoCurso + ", nombreCurso=" + nombreCurso + ", rut=" + rut
					+ ", nombreAlumno=" + nombreAlumno + ", numeroModulo=" + numeroModulo + ", numeroEvaluacion="
					+ numeroEvaluacion + ", nota=" + nota + ", idNota=" + idNota + ", promedioNota=" + promedioNota
					+ ", promedioCurso=" + promedioCurso + "]";
		}
		public NotasDto(int codigoCurso, String nombreCurso, String rut, String nombreAlumno, int numeroModulo,
				int numeroEvaluacion, double nota, int idNota, double promedioNota, double promedioCurso) {
			this.codigoCurso = codigoCurso;
			this.nombreCurso = nombreCurso;
			this.rut = rut;
			this.nombreAlumno = nombreAlumno;
			this.numeroModulo = numeroModulo;
			this.numeroEvaluacion = numeroEvaluacion;
			this.nota = nota;
			this.idNota = idNota;
			this.promedioNota = promedioNota;
			this.promedioCurso = promedioCurso;
		}
		
		
		
				
				
		
}
