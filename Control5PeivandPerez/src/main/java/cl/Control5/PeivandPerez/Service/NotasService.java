package cl.Control5.PeivandPerez.Service;

import java.util.List;

import cl.Control5.PeivandPerez.Model.DTO.NotasDto;

public interface NotasService {
	public int insert(NotasDto notasDto);
	public NotasDto get(String rut); 
	public int update(NotasDto notasDto); 
	public int delete(int idNota);
	public List<NotasDto> list();
	public NotasDto getPromedioAlumno(NotasDto notasDto);
}
