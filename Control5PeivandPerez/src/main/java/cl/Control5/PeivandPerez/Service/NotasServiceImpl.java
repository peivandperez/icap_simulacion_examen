package cl.Control5.PeivandPerez.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.Control5.PeivandPerez.Model.DAO.NotasDao;
import cl.Control5.PeivandPerez.Model.DTO.NotasDto;

@Service
public class NotasServiceImpl implements NotasService {
	@Autowired
	NotasDao notasDao;

	
	public int insert(NotasDto notasDto) {
		return notasDao.insert(notasDto);
	}

	public NotasDto get(String rut) {
		return notasDao.get(rut);
	}

	
	public int update(NotasDto notasDto) {
		return notasDao.update(notasDto);
	}

	
	public int delete(int idNota) {
		return notasDao.delete(idNota);
	}

	
	public List<NotasDto> list() {
		return notasDao.listCurso();
	}

	@Override
	public NotasDto getPromedioAlumno(NotasDto notasDto) {
		return notasDao.getPromedioAlumno(notasDto);
	}

}
