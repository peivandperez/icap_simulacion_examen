function NotasController(option){
	$("#msg").hide();
	$("#msg").removeClass("alert-success").addClass("alert-danger");

	switch(option){
	
	case "list":
		$.ajax({
			type : "post",
			url : "/notas/list",
			success : function(res){
				$("#notasTable").bootstrapTable('load',res);
				$("#notasTable tbody").on('click','tr', function(){
					$("#codigoCurso").val($(this).find("td:eq(0)").text());
					$("#nombreCurso").val($(this).find("td:eq(1)").text());
					$("#rut").val($(this).find("td:eq(2)").text());
					$("#nombreAlumno").val($(this).find("td:eq(3)").text());
					$("#numeroModulo").val($(this).find("td:eq(4)").text());
					$("#numeroEvaluacion").val($(this).find("td:eq(5)").text());
					$("#nota").val($(this).find("td:eq(6)").text());
					$("#idNota").val($(this).find("td:eq(7)").text())
					.click()
				});
				$("#myModal").modal({show:true})
			},
			error : function(){
				$("#msg").show();
				$("#msg").html("Error en búsqueda de notas");
			}
		});
		break;
		
	case "get":
		$.ajax({
			type : "post",
			url : "/notas/get",
			data: "id=" + $("#id").val(),
			success : function(res){
				if (res== null || res==""){
					$("#msg").show();
					$("#msg").html("Record not found")
				}else{
					$("#id").val(res.id);
					$("#item").val(res.item);
					$("#quantity").val(res.quantity);
					$("#price").val(res.price);
					$("#sale_date").val(res.sale_date);
				}
			},
			error : function(){
				$("#msg").show();
				$("#msg").html("Error on notas search");
			}
		});
		break;
	case "insert":
		var json = 
	{
			
			'idNota' : $("#idNota").val(),
			'codigoCurso' : $("#codigoCurso").val(),
			'rut' 		  : $("#rut").val(),
			'numeroModulo' : $("#numeroModulo").val(),
			'numeroEvaluacion' : $("#numeroEvaluacion").val(),
			'nota' : $("#nota").val()
			
	}
		var postData= JSON.stringify(json);
		
		$.ajax({
			type : "post",
			url : "/notas/insert",
			data: postData,
			contentType : "application/json; charset=utf-8",
			success : function(res){
				if (res== 1){
					$("#msg").removeClass("alert-danger").addClass("alert-success")
					$("#msg").show();
					$("#msg").html("Nota ha sido añadida correctamente")
				}else{
					$("#msg").show();
					$("#msg").html("Error al ingresar nota")
				}
			},
			error : function(){
				$("#msg").show();
				$("#msg").html("Error al ingresar nota");
			}
		});
		break;
		
	case "update":
		var json = 
	{
			
			
			'idNota' : $("#idNota").val(),
			'numeroModulo' : $("#numeroModulo").val(),
			'numeroEvaluacion' : $("#numeroEvaluacion").val(),
			'nota' : $("#nota").val()
	}
		var postData= JSON.stringify(json);
		
		$.ajax({
			type : "post",
			url : "/notas/update",
			data: postData,
			contentType : "application/json; charset=utf-8",
			success : function(res){
				if (res== 1){
					$("#msg").removeClass("alert-danger").addClass("alert-success")
					$("#msg").show();
					$("#msg").html("Nota ha sido actualizada correctamente")
				}else{
					$("#msg").show();
					$("#msg").html("Error al acutalizar nota")
				}
			},
			error : function(){
				$("#msg").show();
				$("#msg").html("Error al actualizar nota");
			}
		});
		break;
		
	case "delete":
		$.ajax({
			type : "post",
			url : "/notas/delete",
			data: "idNota=" + $("#idNota").val(),
			success : function(res){
				if (res==1){
					$("#msg").removeClass("alert-danger").addClass("alert-success")
					$("#msg").show();
					$("#msg").html("Nota eliminada")
				}else{
					$("#msg").show();
					$("#msg").html("Nota no pudo ser eliminada")
				}
			},
			error : function(){
				$("#msg").show();
				$("#msg").html("Error en eliminar nota");
			}
		});
		break;
		
	case "promedioAlumno":
		$.ajax({
			type : "post",
			url : "/notas/getPromedioAlumno",
			//data: "rut=" + $("#rut").val(),
			success : function(res){
				$("#PromedioTable").bootstrapTable('load',res);
				$("#PromedioTable tbody").on('click','tr', function(){
					$("#rut").val($(this).find("td:eq(0)").text());
					$("#promedioAlumno").val($(this).find("td:eq(1)").text())
					.click()
				});
				$("#myModalPromedio").modal({show:true})
			},
			error : function(){
				$("#msg").show();
				$("#msg").html("Error en búsqueda de notas");
			}
		});
	default:
			$("#msg").show();
			$("#msg").html("Opción Inválida");
	}
}